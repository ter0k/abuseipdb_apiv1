First, create a virtual environment:
====================================
$ virtualenv --no-site-packages env \
$ source env/bin/activate

Install the requirements:
=========================
$ pip3 install -r requirements.txt

To use abuseipdb.py: 
=====================
 - Edit config.json
 - set your api Token on token = ""
 - set IPs on ip = [] (exemple : ["85.200.118.35", "185.200.118.36""])
 - python abuseipdb.py
