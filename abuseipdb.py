#--- by ter0k ---#

import json
import requests

def conf(confPath):
    with open(confPath, 'rU') as jsonFile:
        jF=jsonFile.read()
        return json.loads(jF)
CONFIG_PATH = "config.json"
CONFIG = conf(CONFIG_PATH)

TOKEN = CONFIG['token']
LIST_IP = CONFIG['ip']
CATEGORIES = ["NA", "NA", "NA", "Fraud Orders", "DDoS Attack", "FTP Brute-Force", "Ping of Death", "Phishing", "Fraud VoIP", "Open Proxy", "Web Spam", "Email Spam", "Blog Spam", "VPN IP", "Port Scan", "Hacking", "SQL Injection", "Spoofing", "Brute-Force", "Bad Web Bot", "Exploited Host", "Web App Attack", "SSH", "IoT Targeted"]


def checkIP(ip, days= 120):
    '''
    curl -n "https://www.abuseipdb.com/check/[IP]/json?key=[API_KEY]"
    '''

    response = requests.get(
       f"https://www.abuseipdb.com/check/{ip}/json?key={TOKEN}&days={days}"
    ).content

    rep = json.loads(response)
    if rep == []:
        return None
    elif isinstance(rep, list):
        return rep[0]
    elif isinstance(rep, dict):
        return rep
    else:
        return None

def checkIPs(ips):
    for ip in ips:
        cck = checkIP(ip)
        if cck:
            display(cck)
        else:
            print(f'{ip} was found in our database!')

def getCategory(cate):
    return CATEGORIES[cate]

def getCategories(tab):
    categories = []
    for i in tab:
        categories.append(getCategory(i))
    return categories

def display(check):
    ip = check["ip"]
    cat = getCategories(check["category"])
    cou = check["country"]
    wlst= check["isWhitelisted"]
    sco = check["abuseConfidenceScore"]

    print(f'IP: {ip}, Score: {sco}, Category: {cat}, Country: {cou}, Is whitelisted: {wlst}')

if __name__ == '__main__':
    checkIPs(LIST_IP)

